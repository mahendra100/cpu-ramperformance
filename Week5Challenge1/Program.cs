﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Timers;

namespace Week5Challenge1
{
    internal class Program
    {
        private static readonly List<float> AvailableCpu = new List<float>();
        private static readonly List<float> AvailableRam = new List<float>();

        protected static PerformanceCounter CpuCounter;
        protected static PerformanceCounter RamCounter;

        private static void Main(string[] args)
        {
            CpuCounter = new PerformanceCounter
            {
                CategoryName = "Processor",
                CounterName = "% Processor Time",
                InstanceName = "_Total"
            };

            RamCounter = new PerformanceCounter("Memory", "Available MBytes");

            try
            {
                System.Timers.Timer t = new System.Timers.Timer(1200);
                t.Elapsed += new ElapsedEventHandler(TimerElapsed);
                t.Start();
                Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine("catched exception");
            }
            Console.ReadLine();
        }

        public static void TimerElapsed(object source, ElapsedEventArgs e)
        {
            // Phase 1 quesrion 1
            float cpu = CpuCounter.NextValue();
            float ram = RamCounter.NextValue();
            Console.WriteLine(string.Format("CPU Value: {0}, ram value: {1}", cpu, ram));

            // Phase 3 question 1
            while (cpu >= 90)
            {
                try
                {
                    SmtpClient client = new SmtpClient("mysmtpserver")
                    {
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential("username", "password")
                    };
                    MailMessage mailMessage = new MailMessage { From = new MailAddress("sender_email@gmail.com") };
                    mailMessage.To.Add("receiver_emil@gmail.com");
                    mailMessage.Body = "Hello";
                    mailMessage.Subject = "Test";
                    client.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    // ignored
                }
            }
            // Phase 3 question 2
            float ramPercentage = (ram / 16384) * 100;
            while (ramPercentage >= 50)
            {
                try
                {
                    SmtpClient client = new SmtpClient("mysmtpserver")
                    {
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential("username", "password")
                    };
                   MailMessage mailMessage = new MailMessage { From = new MailAddress("sender_email@gmail.com") };
                    mailMessage.To.Add("receiver_emil@gmail.com");
                    mailMessage.Body = "Hello";
                    mailMessage.Subject = "Test";
                    client.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    // ignored
                }
            }

            AvailableCpu.Add(cpu);
            AvailableRam.Add(ram);
        }
    }
}